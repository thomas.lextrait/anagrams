/**
 * trietree.h
 *
 * Thomas Lextrait
 * thomas.lextrait@gmail.com
 */

// Largest character code (integer)
#define MAX_CHARSET 255
#define MAX_WORD_LENGTH 128

/**
* TrieTreeNode
*/
class TTNode{
public:
	TTNode(char c, bool isWordEndNode, TTNode* parent);
	static TTNode* makeNode(char c, bool isWordEndNode, TTNode* parent);
	bool hasChild(char c);
	bool hasChildren();
	int getMaxChildIndex();
	bool addChild(char c, bool isWordEndNode);
	TTNode* getChild(char c);
	TTNode* getNextChild(char c);
	TTNode* getFirstChild();
	bool hasNextChild(char c);
	bool isEndOfWord();
	char getValue();
	TTNode* getParent();
	bool hasParent();
private:
	TTNode* parentNode;		// store the parent
	char value;				// character for this node
	bool isWord;			// indicates if this node is the end of a word
	TTNode** children; 		// array of children
	int child_count;		// number of children
	int max_child_index;	// largest index held by a child
};

/**
* TrieTree class
*/
class TrieTree
{
public:
	TrieTree();
	bool addBranch(char* word);
	bool hasBranch(char* word);
	TTNode* getWordNode(char* word);
	char* getNextWord();
	bool hasNextWord();
	void resetIterator();
	int tt_total_nodes;	// stat purposes
private:
	TTNode* root;
	int word_count;
  	char* cur_word; 	// iterator: current word
	int cur_word_num;	// iterator: current word number
};

/**
* Converts a char to lower case
* @param character
* @return lower case character
*/
char lowerCase(char c);

/**
* Converts a word to lower case
* @param word
* @return lower case word
*/
char* lowerCaseWord(char* word);
